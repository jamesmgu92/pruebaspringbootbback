package com.prueba.demo.controller;

import com.prueba.demo.model.Product;
import com.prueba.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("/index")
public class Controller {

    @Autowired
    private ProductService productService;

    //listartodo
    @GetMapping("/product")
    public List<Product> listAll(){
        return productService.listAll();
    }
    //Guardar
    @PostMapping("/product")
    public Product insertProduct(@RequestBody Product obj){
        return productService.insertUpdateProduct( obj);
    }
    //buscar por id
    @GetMapping("/product/{id}")
    public Optional<Product> searchId(@PathVariable Integer id){
        return productService.searchId(id);
    }
    //modificar
    @PutMapping("/product/{id}")
    public Product UpdateProduct(@RequestBody Product obj, @PathVariable Integer id){
        Optional<Product>  productActual=productService.searchId(id);
        Product productModify= productActual.get();
        productModify.setCreation_date(obj.getCreation_date());
        productModify.setDescription(obj.getDescription());
        productModify.setExpiry_date(obj.getExpiry_date());
        productModify.setName(obj.getName());
        productModify.setPrice(obj.getPrice());

        return productService.insertUpdateProduct(productModify);

    }

    //eliminar
    @DeleteMapping("/product/{id}")
    public void delete(@PathVariable Integer id){
        productService.deleteProduct(id);
    }
}
