package com.prueba.demo.Dao;

import com.prueba.demo.model.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductDao extends CrudRepository<Product, Integer> {

    @Query(value = "select * from product  where like :var_name;", nativeQuery = true)
    public abstract List<Product> listName(@Param("var_name") String name);
    @Query(value = "select * from product  where price <= :var_price;", nativeQuery = true)
    public  List<Product> price(@Param("var_price") Double price);
}
