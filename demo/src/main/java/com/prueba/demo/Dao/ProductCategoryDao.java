package com.prueba.demo.Dao;

import com.prueba.demo.model.ProductCategory;
import org.springframework.data.repository.CrudRepository;

public interface ProductCategoryDao extends CrudRepository<ProductCategory, Integer> {
}
