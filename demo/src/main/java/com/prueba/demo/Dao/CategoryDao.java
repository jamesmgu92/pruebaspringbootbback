package com.prueba.demo.Dao;

import org.springframework.data.repository.CrudRepository;
import com.prueba.demo.model.Category;

public interface CategoryDao extends CrudRepository<Category, Integer> {
}
