package com.prueba.demo.service;

import com.prueba.demo.Dao.CategoryDao;
import com.prueba.demo.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class CategoryServiceImpl implements CategoryService{

    @Autowired
    private CategoryDao repository;
    @Override
    @Transactional(readOnly = true)
    public List<Category> listCategory() {
        return (List<Category>) repository.findAll();
    }
}
