package com.prueba.demo.service;

import com.prueba.demo.Dao.ProductDao;
import com.prueba.demo.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductDao repository;

    @Override

    public Product insertUpdateProduct(Product obj) {
        return repository.save(obj);
    }

    @Override

    public void deleteProduct(int id) {

        repository.deleteById(id);

    }

    @Override
    @Transactional(readOnly = true)
    public List<Product> listAll() {
        return (List<Product>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Product> listName(String name) {
        return repository.listName(name);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Product> searchId(int id) {
        return repository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Product> price(Double price) {
        return repository.price(price);
    }
}
