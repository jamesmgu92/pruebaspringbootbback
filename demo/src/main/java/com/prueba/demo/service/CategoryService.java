package com.prueba.demo.service;

import com.prueba.demo.model.Category;

import java.util.List;

public interface CategoryService {

    public abstract List<Category> listCategory();

}
