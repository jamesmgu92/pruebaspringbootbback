package com.prueba.demo.service;

import com.prueba.demo.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    public abstract Product insertUpdateProduct(Product obj);
    public abstract void deleteProduct(int id);
    public abstract List<Product> listAll();
    public abstract List<Product> listName(String name);
    public abstract Optional<Product> searchId(int id);
    public abstract List<Product> price(Double price);
}
